# Programa per enviar la ip d'este ordinador a un grup de telegram

# Llibreries
import requests
import os

# Configuració
id_bot = "YOUR_BOT_ID"
id_xat = "YOUR_CHAT_ID"

# Obtenció de la ip
ip = os.popen('hostname -I').read()

# Espai disponible i consumit
gastat = os.popen("df -k -h /tmp | tail -1 | awk '{print $3}'").read().replace("\n","")
disp = os.popen("df -k -h /tmp | tail -1 | awk '{print $4}'").read()
percent = os.popen("df -k -h /tmp | tail -1 | awk '{print $5}'").read().replace("\n","")


# Construcció de l'enllaç
url = "https://api.telegram.org/bot"+id_bot+"/sendMessage?chat_id="+id_xat+"&text=🤖🤖Homero🤖🤖\nLa IP és: "+ip+"Espai consumit: "+gastat+" ("+percent+")\nEspai disponible: "+disp

# Enviament
requests.post(url)
