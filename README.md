# Automated dynamic IP sender for Linux

These tools provide and automatic way to send the IP of one computer to a Telegram group at start up.

This can be useful if you need to connect to a remote computer and it has a dynamic IP. With this tool, you can enable a _systemd_ service to automate the sending of the IP of your computer to a desired Telegram group.

## Capabilities

This service runs every time that your computer turns on and call a python script that sends your IP adress to a desired Telegram group. The sending message can be configured.

This service requieres python (or python3) with the module _requests_.

Tested on: _Xubuntu 22.04.3_

## Setting up

_You can download this guide and files via_ ```git clone https://gitlab.com/escanyapobres/automated-dynamic-ip-sender-for-linux.git``` _or follow these steps:_

Create a service file (rename it if you want):
```sh
sudo nano /etc/systemd/system/telegram-envia-ip.service
```

And write:

```sh
[Unit]
After = network.target network-online.target dbus.service
Wants = network-online.target
Requires = dbus.service

[Service]
ExecStart=/usr/local/bin/telegram-envia-ip.sh

[Install]
WantedBy=default.target
```
The lines _After, Wants_ and _Requires_ ensure that the service will have internet connection.

Now, let's create the executable bash script:
```sh
sudo nano /usr/local/bin/telegram-envia-ip.sh
```
And write:
```sh
#!/bin/sh

/usr/bin/python3 "/usr/local/bin/enviador.py"
```
This will run the Python program with the instructions.

Let's write the Python script by:
```sh
sudo nano /usr/local/bin/enviador.py
```

And write:
```py
# Programa per enviar la ip d'este ordinador a un grup de telegram

# Llibreries
import requests
import os

# Configuració
id_bot = YOUR_BOT_ID
id_xat = "YOUR_GROUP_CHAT_ID"

# Obtenció de la ip
ip = os.popen('hostname -I').read()

# Espai disponible i consumit
gastat = os.popen("df -k -h /tmp | tail -1 | awk '{print $3}'").read().replace("\n","")
disp = os.popen("df -k -h /tmp | tail -1 | awk '{print $4}'").read()
percent = os.popen("df -k -h /tmp | tail -1 | awk '{print $5}'").read().replace("\n","")


# Construcció de l'enllaç
url = "https://api.telegram.org/bot"+id_bot+"/sendMessage?chat_id="+id_xat+"&text=🤖🤖Homero🤖🤖\nLa IP és: "+ip+"Espai consumit: "+gastat+" ("+percent+")\nEspai disponible: "+disp

# Enviament
requests.post(url)
```

After that we change user permissions of the created files by (you can check the [chmod codes here](https://chmodcommand.com/chmod-744/)):

```sh
sudo chmod 744 /usr/local/bin/enviador.py
sudo chmod 744 /usr/local/bin/telegram-envia-ip.sh
sudo chmod 664 /etc/systemd/system/telegram-envia-ip.service
```

Finally reload the demon and load the sender service:
```sh
sudo systemctl daemon-reload
sudo systemctl enable telegram-envia-ip.service
```

Result:

![Message sended by the automated bot](resultat.png)
